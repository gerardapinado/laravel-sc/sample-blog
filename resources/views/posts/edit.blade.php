@extends('layouts.app')

@section('content')
   <h1>Edit post</h1>

   <form action="{{action('PostController@update', [$post->id] )}}" method="POST">
      @csrf
      {{-- hidden input --}}
      <input type="hidden" name="_method" id="" value="PUT">
      <div class="form-group">
         <label for="title-input">Title</label>
         <input id='title-input' type="text" name="title" class='form-control' placeholder="Title" value="{{$post->title}}">
      </div>
      <div class="form-group"> 
         <label for="body-input">Body</label>
         <textarea class="form-control" name="body" id="body-input" rows="5">{{$post->body}}</textarea>
      </div>
      <button type="submit" class="btn btn-primary my-3">Edit</button>
   </form>
@endsection
