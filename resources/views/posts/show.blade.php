@extends('layouts.app')

@section('content')
    <h1>{{ $post->title}}</h1>
    <p>Written on {{$post->created_at}}</p>

    <div class="">
        {{$post->body}}
    </div>

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-secondary my-4" data-bs-toggle="modal" data-bs-target="#exampleModal">
        Add Comment
        </button>
      
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">  
                <h5 class="modal-title" id="exampleModalLabel">{{$post->title}}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                {{-- Comment Form --}}
                <form action="{{action('PostController@comment', [$post->id])}}" method="POST" class="d-flex">
                    @csrf
                    <input type="hidden" name="_method" value='POST'>
                    <label for="comment">Write a Comment: </label>
                    <textarea name="comment" id="comment" cols="50" rows="10"></textarea>
                </form>
            
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button href='/posts/{id}/comment' type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
            </div>
        </div>

    <div class="d-flex">
        <a href="/posts/{{$post->id}}/edit" class='btn btn-primary me-3'>Edit</a>

        <form action="{{action('PostController@destroy', [$post->id])}}" method="POST">
            @csrf
            <input type="hidden" name="_method" value='DELETE'>
            <a href='/posts/{{$post->id}}' class='btn btn-primary btn-danger'>Delete</a>
        </form>
    </div>
    
@endsection