@extends('layouts.app')

@section('content')
   <h1>This is the create page</h1>

   <form action="{{ action('PostController@store') }}" method="POST">
      @csrf
      <div class="form-group">
         <label for="title-input">Title</label>
         <input id='title-input' type="text" name="title" class='form-control' placeholder="Title">
      </div>
      <div class="form-group"> 
         <label for="body-input">Body</label>
         <textarea class="form-control" name="body" id="body-input" rows="5"></textarea>
      </div>
      <a type="submit" class="btn btn-primary">Submit</a>
   </form>
@endsection
