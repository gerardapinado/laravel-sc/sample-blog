<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', 'PageController@hello' );

Route::get('/index', function(){
    return view('pages.index');
});

Route::get('/about', function(){
    return view('pages.about');
});

Route::get('/services', function(){
    return view('pages.services');
});

Route::resource('/posts', 'PostController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/posts/{id}/comment', 'PostsController@comment');
