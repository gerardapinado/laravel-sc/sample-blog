<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function hello () {
        // return view('hello')->with('name', 'Gerard');
        $info = array(
            'front_end'=>'Zuitt Coding Bootcamp',
            'topics' => ['HTML', 'CSS', 'JS', 'PHP', 'MySQL']
        );

        return view('hello')->with($info);
    }

    public function index () {
        $content = array(
            'title' => 'Welcome to my site',
            'desc' => 'This website is created using laravel'
        );
        return view('pages/index')->with($content);
    }

    public function about () {
        $content = array(
            'title' => 'About',
            'desc' => 'This is the about section'
        );
        return view('pages/index')->with($content);
    }

    public function services () {
        $content = array(
            'title' => 'Services',
            'desc' => 'This is the service section'
        );
        return view('pages/index')->with($content);
    }
}
